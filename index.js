console.log("Hello world!");
// enclose with quotation mark string datas

console. log("Hello world!");
// case sensitive

console.
log
(
    "Hello, everyone!"
);

// ;delimeter
// we use delimeter to end our code

// Syntax and statement

// Statements in programming are instruction that we tell to computer to perform
// Syntax in programming, it is the set of rules that describes how statement must be considered

// Variables

/*
    it is used to contain data 
    -Syntax is declaring variables 
    -let/const variableName
 */

let myVariable ="Hello";
console.log(myVariable);


// console.log(hello); // will result to not defined error

// String
let productName = 'desktop computer';
console.log(productName)

let product = "Chester's computer";
console.log(product)


// Number
let productPrice =18999 ;
console.log(productPrice);

const interest = 3.539;
console.log(interest);


// Reassigniung variable value
// Syntax
    //variableName = newValue;

productName = "Laptop";
console.log(productName)

let friend = "Dann";
friend = "Xyd";
console.log(friend);

// interest = 4.89;

const pi = 3.14;
console.log(pi)
// pi =

// Reassigning a variable already have a value and we re-assign a new one
// Initialize - it is our first saving of a value

let supplier; //declaration
supplier = "John Smith Trading"; //initializing
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// Multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productBrand, productCode );

// Using a variable with a reserved keyword
// const let = "hello";
// console.log(let);

// [SECTION] Data Types

// String
// String are series of characters thatt create a word, phrase, sentence, or anything related to creating text.

// String in JavaScript is enclosed with a single ('') or double ("") quote

let country = 'Philippines';
let province = "Metro Manila";

let fullAddress = province +  ', ' + country;
// We use + symbol to concatenate data / values

console.log(province + ' , ' + country);
console.log(fullAddress)

console.log("Philippines" + ', ' + "Metro Manila");

// Escape Characters (\)
// "\n" refers to creating a new line or set the text to next line;

console.log("line1\nline2")

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress)

let message = "John's employee went home early.";
console.log(message);

message = 'John\'s employee went home early';
console.log(message);


let headcount = 26;
console.log(headcount);


let grade = 98.7
console.log(grade);

let planetDistance =2e10
console.log(planetDistance);

console.log ("John's grade last quarter is "+ grade);

let grades = [98.7, 95.4, 90.2, 94.6];
console.log(grades)

let details = ["John", "Smith", 32, true];
console.log(details);

//Objects 
//Objects are another special kind of data type that is used to mimic real world objects items.

// Syntax:
/* 
let/const objectName = {
    propertyA: value,
    propertyB: value

}

*/

let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: ["0912 3455 6789", "8123 7444"],
    // address: {
    //     houseNumber:"345",
    //     city: "Manila"
    // }
};

console.log(person);

const myGrades = {
    firstGrading: 98.7,
    secondGrading: 95.4,
    thirdGrading: 90.2,
    fourthGrading: 94.6
};

console.log(myGrades);

let stringValue = "abcd";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 640 , 700];


console.log(typeof stringValue);
console.log(typeof numberValue);
console.log(typeof booleanValue);
console.log(typeof waterBills);
console.log(typeof myGrades);


const anime = ["Boruto", "One Piece", "Code Geas", "Dan Maci", "Demon Slayer", "AOT", "Fairy Tale"];
anime[0] = "Naruto"
console.log(anime);

//Null

let spouse = null;
console.log(spouse);

// Undefined

let fullName;
console.log(fullName)
